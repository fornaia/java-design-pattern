package mediator.after;

public interface Mediator {
	void taskCompleted(String task);
	void loadValues();
	void show();
}

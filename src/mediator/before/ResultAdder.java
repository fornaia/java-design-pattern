package mediator.before;

import java.util.List;

public class ResultAdder {
	Visualiser vis;
	
	public ResultAdder(Visualiser vis) {
		this.vis = vis;
	}

	public void add(List<Integer> values) {
		int sum = values.stream().reduce(0, Integer::sum);
		vis.setSum(sum);
	}

}

package mediator.before;

public class TestVisualiser {

	public static void main(String[] args) {
		Visualiser v = new Visualiser();
		ResultCounter rc = new ResultCounter(v);
		ResultAdder ra = new ResultAdder(v);
		Loader l = new Loader(rc, ra);

		v.show();

		l.loadValues();
		v.show();
	}

}

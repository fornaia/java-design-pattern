package mediator.before;

import java.util.List;

public class ResultCounter {
	Visualiser vis;
	
	public ResultCounter(Visualiser vis) {
		this.vis = vis;
	}

	public void count(List<Integer> values) {
		int cnt = (int) values.stream().count();
		vis.setCount(cnt);
	}

}

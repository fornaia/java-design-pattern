package mediator.before;

import java.util.List;

public class Loader {
	ResultCounter rc;
	ResultAdder ra;
	
	List<Integer> values;
	
	public Loader(ResultCounter rc, ResultAdder ra) {
		this.ra = ra;
		this.rc = rc;
	}
	
	public void loadValues() {
		values = List.of(10, 20, 15, 13);
		rc.count(values);
		ra.add(values);
	}

}
